-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 31, 2019 at 05:10 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `roumtho`
--

-- --------------------------------------------------------

--
-- Table structure for table `dish`
--

CREATE TABLE `dish` (
  `id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `d_name` varchar(50) NOT NULL,
  `s_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dish`
--

INSERT INTO `dish` (`id`, `d_id`, `d_name`, `s_id`) VALUES
(1, 1011, 'จานที่ 1', 1001),
(2, 1012, 'จานที่ 2', 1001),
(3, 1013, 'จานที่ 3', 1001),
(4, 1014, 'จานที่ 4', 1001),
(5, 1015, 'จานที่ 5', 1001),
(6, 1016, 'จานที่ 6', 1001),
(7, 1017, 'จานที่ 7', 1001),
(8, 1018, 'จานที่ 8', 1001);

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `f_id` int(11) NOT NULL,
  `f_name` varchar(50) NOT NULL,
  `d_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`f_id`, `f_name`, `d_id`) VALUES
(1, 'ข้าวเกียบ', 1011),
(2, 'ขนมจีน', 1012),
(3, 'ใส้กรอก', 1012),
(4, 'แฮม', 1012),
(5, 'หมูแผ่น', 1012),
(6, 'กระเพาะปลาน้ำแดงทรงเครื่อง', 1013),
(7, 'ยำทะเล', 1014),
(8, 'ยำสามกรอก', 1014),
(9, 'กุ้งอบวุ้นเส้น', 1014),
(10, 'สลัดปลาทิพย์', 1014),
(11, 'สลัดกุ้งทอด', 1014),
(12, 'ปลาทับทิมทอดยำมะม่วง', 1015),
(13, 'ปลาทับทิมสามรส', 1015),
(14, 'ปลาทับทิมนึ่งมะนาว', 1015),
(15, 'ปลาทับทิมนึ่งบ๊วย', 1015),
(16, 'ปลาทับทิมน้ำปลา', 1015),
(17, 'ข้าวผัดปูใส้กรอก', 1016),
(18, 'ข้าวผัดปู', 1016),
(19, 'ข้าวผัดไก่', 1016),
(20, 'ข้าวผัดปหมู', 1016),
(21, 'ต้มยำทะเลน้ำข้นรวมมิตร', 1017),
(22, 'ขาหมูต้มยำ', 1017),
(23, 'แก้งส้มชะอมไข่ทอดไส่กุ้ง', 1017),
(24, 'วุ้นมะพร้าวอ่อน', 1018),
(25, 'ข้าวเหนียวเผือก', 1018),
(26, 'รวมมิตรไทย', 1018),
(27, 'เต้าฮวยฟลุตสลัด', 1018);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `mb_id` int(11) NOT NULL,
  `mb_username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mb_password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mb_prefix` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `mb_firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mb_lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mb_birthday` date DEFAULT NULL,
  `mb_age` int(11) DEFAULT NULL,
  `mb_sex` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mb_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mb_province` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mb_zipcode` int(4) DEFAULT NULL,
  `mb_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mb_telphone` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `mb_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mb_date_register` datetime NOT NULL,
  `mb_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`mb_id`, `mb_username`, `mb_password`, `mb_prefix`, `mb_firstname`, `mb_lastname`, `mb_birthday`, `mb_age`, `mb_sex`, `mb_address`, `mb_province`, `mb_zipcode`, `mb_email`, `mb_telphone`, `mb_image`, `mb_date_register`, `mb_type`) VALUES
(14, 'tuta', '1234', 'นาย', 'ปิยนัท', 'กิตสเนท', '2019-10-09', 20, 'ชาย', '15 ต.ตลาด อ.เมือง', 'อุบลราชธานี', 40000, 'test@test.com', '0885454458', 'https://i3.wp.com/www.catdumb.com/wp-content/uploads/2019/03/54432777_636578616814745_5260691723297751040_n.jpg', '2019-10-09 00:00:00', 'customer'),
(17, 'field', '1234', 'นาย', 'กฤติกร', 'จิตประภาจิณ', NULL, NULL, 'ชาย', '1112/71 ถ.ผังเมืองบัญชา ต.ตลาด อ.เมือง', 'จ.มหาสารคาม', 44000, 'feidl_1998@hotmail.com', '0696535545', NULL, '2019-10-16 00:00:00', 'customer');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `od_id` int(11) NOT NULL,
  `od_datetime` datetime NOT NULL,
  `or_blank` varchar(30) NOT NULL,
  `od_table` int(11) NOT NULL,
  `od_amount_paid` int(11) NOT NULL,
  `od_proof_payment` varchar(255) NOT NULL,
  `od_status` varchar(20) NOT NULL DEFAULT '''รอการชำระ''',
  `od_mb_id` int(11) NOT NULL,
  `od_st_id` int(11) NOT NULL,
  `od_s_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`od_id`, `od_datetime`, `or_blank`, `od_table`, `od_amount_paid`, `od_proof_payment`, `od_status`, `od_mb_id`, `od_st_id`, `od_s_id`) VALUES
(1, '2019-10-16 00:00:00', '', 10, 1100, '', 'รอการชำระ', 1, 1, 1001);

-- --------------------------------------------------------

--
-- Table structure for table `order_menu`
--

CREATE TABLE `order_menu` (
  `odm_id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `f_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `set`
--

CREATE TABLE `set` (
  `id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `s_name` varchar(50) NOT NULL,
  `s_price` int(11) NOT NULL,
  `s_image` varchar(255) NOT NULL,
  `d_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `set`
--

INSERT INTO `set` (`id`, `s_id`, `s_name`, `s_price`, `s_image`, `d_id`) VALUES
(1, 1001, 'ชุดที่ 1', 1100, 'http://localhost/roumtho/assets/menu-1100.png', 1011),
(2, 1002, 'ชุดที่ 2', 2000, 'http://localhost/roumtho/assets/menu-2000.png', 1021),
(3, 1003, 'ชุดที่ 3', 2700, 'http://localhost/roumtho/assets/menu-2700.png', 1031),
(4, 1004, 'ชุดที่ 4', 3800, 'http://localhost/roumtho/assets/menu-2700.png', 1041);

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `st_id` int(11) NOT NULL,
  `st_name` varchar(30) NOT NULL,
  `st_location` varchar(255) NOT NULL,
  `st_province` varchar(55) NOT NULL,
  `st_zipcode` int(4) NOT NULL,
  `st_type` varchar(50) NOT NULL,
  `st_image` varchar(255) DEFAULT NULL,
  `st_score` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`st_id`, `st_name`, `st_location`, `st_province`, `st_zipcode`, `st_type`, `st_image`, `st_score`) VALUES
(1, 'โภชนาอาหารจีน', '11 ต.ตลาด อ.เมือง', 'มหาสารคาม', 44000, 'อาหารจีน', 'http://localhost/roumtho/assets/Noodle_Asia.0.0.0.0.0.jpg', 0),
(2, 'แดงโภชนา', '59 ต.ตลาด อ.เมือง', 'ขอนแก่น', 40000, 'อาหารไทย', 'http://localhost/roumtho/assets/Noodle_Asia.0.0.0.0.0.jpg', 5),
(3, 'ก้องโภชนา', '9 ต.ตลาด อ.เมือง', 'ชุมแพ', 40000, 'อาหารโบราญ', 'http://localhost/roumtho/assets/Noodle_Asia.0.0.0.0.0.jpg', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dish`
--
ALTER TABLE `dish`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`mb_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`od_id`);

--
-- Indexes for table `set`
--
ALTER TABLE `set`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`st_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dish`
--
ALTER TABLE `dish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `mb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `od_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `set`
--
ALTER TABLE `set`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `st_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
