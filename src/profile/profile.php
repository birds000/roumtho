<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>List</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../../css/card.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

    <?php
    include("../connectDB.php");

    $sql = "SELECT * FROM member WHERE mb_id = 14";
    $result = $db->query($sql);

    $row = $result->fetch_array(MYSQLI_BOTH);
    ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <!--navbar-->
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand text-white" href="../../index.php">
                <img src="/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
                รวมโต๊ะ . com
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="../../index.php">หน้าแรก</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../list.php">บริเวณใกล้เคียง
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../list.php">ร้านแนะนำ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../order.php">รายการจอง
                            <span class="badge badge-light">1</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">ติดต่อเรา</a>
                    </li>
                    <?php
                    if (!isset($_SESSION['id'])) {
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" href="login.html">เข้าสู่ระบบ/สมัครสมาชิก</a>
                        </li>
                    <?php
                    } else {
                        ?>
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                โปรไฟล์
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">ชื่อผู้ใช้</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="../login/logout.php">ออกจากระบบ</a>
                            </div>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

    <br>
    <!--content-->
    <div class="container">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col">
                        <h4>โปรไฟล์</h4>
                    </div>
                    <div class="col text-right">
                        <button class="btn"><i class="fa fa-edit" data-toggle="modal" data-target="#edit_profile"></i></button>
                    </div>
                </div>

                <img src="<?php echo $row['mb_image']; ?>" class="rounded mx-auto d-block" alt="..." style="width: 20rem;">

                <br>

                <form>
                    <div class="form-group row">
                        <div class="col">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" placeholder="username" value="<?php echo $row['mb_username']; ?>" disabled>
                        </div>
                        <div class="col">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="lastname" placeholder="password" value="<?php echo $row['mb_password']; ?>" disabled>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-2">
                            <label for="firstname">คำนำหน้าชื่อ</label>
                            <input type="text" class="form-control" id="firstname" placeholder="ชื่อจริง" value="<?php echo $row['mb_prefix']; ?>" disabled>
                        </div>
                        <div class="col-5">
                            <label for="firstname">ชื่อจริง</label>
                            <input type="text" class="form-control" id="firstname" placeholder="ชื่อจริง" value="<?php echo $row['mb_firstname']; ?>" disabled>
                        </div>
                        <div class="col-5">
                            <label for="lastname">นามสกุล</label>
                            <input type="email" class="form-control" id="lastname" placeholder="นามสกุล" value="<?php echo $row['mb_lastname']; ?>" disabled>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <label for="sex">เพศ</label>
                            <input type="text" class="form-control" id="sex" value="<?php echo $row['mb_sex']; ?>" disabled>
                        </div>
                        <div class="col">
                            <label for="birthday">วันเดือนปีเกิด</label>
                            <input type="date" class="form-control" id="birthday" value="<?php echo $row['mb_birthday']; ?>" disabled>
                        </div>
                        <div class="col">
                            <label for="age">อายุ</label>
                            <input type="number" class="form-control" id="age" placeholder="อายุ" value="<?php echo $row['mb_age']; ?>" disabled>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Email" value="<?php echo $row['mb_email']; ?>" disabled>
                        </div>
                        <div class="col">
                            <label for="phone">เบอร์โทร</label>
                            <input type="text" class="form-control" id="phone" placeholder="เบอร์โทร" value="<?php echo $row['mb_telphone']; ?>" disabled>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <label for="adress">ที่อยู่</label>
                            <textarea type="text" class="form-control" id="adress" placeholder="..." disabled><?php echo $row['mb_address']; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <label for="province">จังหวัด</label>
                            <input type="text" class="form-control" id="province" placeholder="จังหวัด" value="<?php echo $row['mb_province']; ?>" disabled>
                        </div>
                        <div class="col">
                            <label for="zibcode">รหัสไปรษณีย์</label>
                            <input type="number" class="form-control" id="zibcode" placeholder="รหัสไปรษณีย์" value="<?php echo $row['mb_zibcode']; ?>" disabled>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--content-->
    <br>
    <br>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-xl" id="edit_profile" tabindex="-1" role="dialog" aria-labelledby="edit_profile" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">แก้ไขโปรไฟล์</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group row">
                            <div class="col">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" id="username" placeholder="username" disabled>
                            </div>
                            <div class="col">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="lastname" placeholder="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="firstname">คำนำหน้าชื่อ</label>
                                <input type="text" class="form-control" id="firstname" placeholder="ชื่อจริง" value="">
                            </div>
                            <div class="col">
                                <label for="firstname">ชื่อจริง</label>
                                <input type="text" class="form-control" id="firstname" placeholder="ชื่อจริง">
                            </div>
                            <div class="col">
                                <label for="lastname">นามสกุล</label>
                                <input type="email" class="form-control" id="lastname" placeholder="นามสกุล">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col">
                                <label for="sex">เพศ</label>
                                <input type="text" class="form-control" id="sex" value="เพศ" disabled>
                            </div>
                            <div class="col">
                                <label for="birthday">วันเดือนปีเกิด</label>
                                <input type="date" class="form-control" id="birthday" value="">
                            </div>
                            <div class="col">
                                <label for="age">อายุ</label>
                                <input type="number" class="form-control" id="age" placeholder="อายุ" value="" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" placeholder="Email">
                            </div>
                            <div class="col">
                                <label for="phone">เบอร์โทร</label>
                                <input type="text" class="form-control" id="phone" placeholder="เบอร์โทร">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col">
                                <label for="adress">ที่อยู่</label>
                                <textarea type="text" class="form-control" id="adress" placeholder="..."></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col">
                                <label for="province">จังหวัด</label>
                                <input type="text" class="form-control" id="province" placeholder="จังหวัด">
                            </div>
                            <div class="col">
                                <label for="zibcode">รหัสไปรษณีย์</label>
                                <input type="number" class="form-control" id="zibcode" placeholder="รหัสไปรษณีย์">
                            </div>
                        </div>

                        <div class="form-group custom-file">
                            <input type="file" class="form-control" id="customFile">
                            <label class="custom-file-label" for="customFile">เลือกรูปภาพโปรไฟล์</label>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
                    <button type="button" class="btn btn-primary">บันทึก</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="page-footer font-small bg-dark" style="color: white">
        <div class="container">
            <div class="container-fluid text-center text-md-left">
                <hr>
                <div class="row">
                    <div class="col-md-6 mt-md-0 mt-6">
                        <h3 class="text-uppercase text-danger">รวมโต๊ะ</h3>
                        <h3 class="text-uppercase text-danger">.COM</h3>
                        <hr>
                        <p>ศูนย์รวมการจัดงานเลี้ยง ค้นหาเมนูที่คุณต้องการในราคาที่ถูกใจ</p>
                        <p>ได้ที่ รวมโต๊ะ.com เรายินดีให้คำแนะนำ</p>
                    </div>
                    <hr class="clearfix w-100 d-md-none pb-6">
                    <div class="col-md-6 mb-md-0 mb-6">
                        <br></br>
                        <ul class="list-unstyled">
                            <li>
                                <a href="../../index.php" class="text-white text-decoration-none">หน้าแรก</a>
                            </li>
                            <li>
                                <a href="../list.php" class="text-white text-decoration-none">บริเวณใกล้เคียง</a>
                            </li>
                            <li>
                                <a href="../list.php" class="text-white text-decoration-none">ร้านแนะนำ</a>
                            </li>
                            <li>
                                <a href="#!" class="text-white text-decoration-none">ติดต่อเรา</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="https://รวมโต๊ะ.com" class="text-danger text-decoration-none"> รววมโต๊ะ.com</a>
            </div>
        </div>
    </footer>

</body>

</html>