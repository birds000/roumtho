<?php
    session_start();
?>
<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/card.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

    <?php
        include("connectDB.php");

        $sql_store = "SELECT * FROM store WHERE st_id = 1";
        $result_store = $db_roumtho->query($sql_store);
        $row_store = $result_store->fetch_array(MYSQLI_BOTH);

        $sql_order = "SELECT * FROM `order`";
        $result_order = $db_roumtho->query($sql_order);
        $row_order = $result_order->fetch_array(MYSQLI_BOTH);

        $sql_food = "SELECT * FROM `food` WHERE d_id = 1011";
        $result_food = $db_roumtho->query($sql_food);
        $row_food = $result_food->fetch_array(MYSQLI_BOTH);
    ?>

    <!--navber-->
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand text-white" href="../index.php">
                <img src="/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30"
                    class="d-inline-block align-top" alt="">
                รวมโต๊ะ . com
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav ml-auto">
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="list.php"> <i class="fa fa-map-marker" aria-hidden="true"></i> บริเวณใกล้เคียง</a>
                    </li> -->
                    <li class="nav-item">
                        <a href="../index.php#restaurant_popular" class="nav-link" href="list.php"><i class="fa fa-tags"></i> ร้านแนะนำ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="order.php"><i class="fa fa-shopping-cart"></i> รายการจอง
                            <span class="badge badge-light">1</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="#footer" class="nav-link" href="#"><i class="fa fa-phone"></i> ติดต่อเรา</a>
                    </li>
                    <?php
                        if (!isset($_SESSION['id'])) {
                            ?>
                    <li class="nav-item">
                        <a class="btn btn-danger" href="login.html"><i class="fa fa-sign-in"></i> เข้าสู่ระบบ</a>
                    </li>
                    <?php
                        }else {
                            ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            โปรไฟล์
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="profile/profile.php">ชื่อผู้ใช้</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="login/logout.php">ออกจากระบบ</a>
                        </div>
                    </li>
                    <?php
                        }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

    <!--body-->
    <div class="container" style="height: 900px;">

        <br>
        <div class="alert alert-light" role="alert">
            หน้าแรก / รายการจอง
        </div>

        <a href="payment.php" class="card mb-3 text-decoration-none" style="color: black;">
            <div class="row no-gutters">
                <div class="col-md-4">
                    <img src="<?php echo $row_store['st_image']; ?>"
                        class="card-img" alt="image" style="height: 100%">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-7">
                                <h5 class="card-title">ชื่อร้าน : <?php echo $row_store['st_name']; ?></h5>
                                <strong>ประเภทร้าน : <?php echo $row_store['st_type']; ?></strong>
                            </div>
                            <div class="col-5">
                                <p class="card-text" style="text-align: right;"><small
                                        class="text-muted"><strong>วันที่เวลาจอง</strong> <?php echo $row_order['od_datetime']; ?></small></p>
                            </div>
                        </div>
                        <p class="card-text"><strong>เมนูอาหาร : </strong> 1.ข้าวเกรียบ 2.ขนมจีน
                            3.กระเพาะปลาน้ำแดงทรงเครื่อง 4.ยำทะเล
                            5.ปลาทับทิมทอดยำมะม่วง 6.ข้าวผัดปูใส้กรอก 7.ต้มยำทะเลน้ำข้นรวมมิตร 8.วุ้นมะพร้าวอ่อน</p>

                        <div class="row">
                            <div class="col-7">
                                <h5 class="card-title">ค่าใช้จ่ายทั้งหมด : <?php echo $row_order['od_total']; ?> บาท</h5>
                            </div>
                            <div class="col-5" style="text-align: right;">
                                <span class="badge badge-pill badge-warning">สถานะ : <?php echo $row_order['od_status']; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <a name="footer"></a>

    <!-- Footer -->
    <footer class="page-footer font-small bg-dark" style="color: white">
        <div class="container">
            <div class="container-fluid text-center text-md-left">
                <hr>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6">
                        <!--title-->
                        <h3 class="text-uppercase text-danger">รวมโต๊ะ</h3>
                        <h3 class="text-uppercase text-danger">.COM</h3>
                        <!--title-->
                        <hr>
                        <!--Detail-->
                        <p>ศูนย์รวมการจัดงานเลี้ยง ค้นหาเมนูที่คุณต้องการในราคาที่ถูกใจ</p>
                        <p>ได้ที่ รวมโต๊ะ.com เรายินดีให้คำแนะนำ</p>
                        <!--Detail-->
                    </div>
                    <hr class="clearfix w-100 d-md-none pb-6">
                    <!--Link-->
                    <div class="col-12 col-md-6 col-lg-3">
                        <br>
                        <ul class="list-unstyled">
                            <li>
                                <a href="index.php" class="text-white text-decoration-none"><i class="fa fa-home"></i> &nbsp;&nbsp;หน้าแรก</a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-shopping-cart"></i> &nbsp;&nbsp;รายการที่จอง <span class="badge badge-light">1</span></a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;บริเวณใกล้เคียง</a>
                            </li>                            
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-tags"></i> &nbsp;ร้านแนะนำ</a>
                            </li>
                            <li>
                                <a href="#!" class="text-white text-decoration-none"><i class="fa fa-phone"></i> &nbsp;&nbsp;ติดต่อเรา</a>
                            </li>
                        </ul>
                    </div>
                    <!--Link-->

                    <div class="col-12 col-md-6 col-lg-3">
                        <br>
                        <p>
                            <a href="!#" class="text-primary" ><i class="fa fa-facebook-square fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-warning" ><i class="fa fa-instagram fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-success" ><i class="fa fa-phone-square fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-danger" ><i class="fa fa-google-plus-square fa-2x"></i></a> &nbsp;
                        </p>
                        
                    </div>
                </div>
            </div>
            <!--Copyright-->
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="https://รวมโต๊ะ.com" class="text-danger text-decoration-none"> รววมโต๊ะ.com</a>
            </div>
        </div>
    </footer>
    <!-- Footer -->

</body>

</html>