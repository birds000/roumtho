<?php
session_start();
?>
<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/card.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
</head>

<body>
    <?php
    include("connectDB.php");

    $sql_store = "SELECT * FROM store WHERE st_id =1001";
    $result_store = $db_con->query($sql_store);
    $row_store = $result_store->fetch_array();

    $sql_set = "SELECT * FROM `set`";
    $result_set = $db_con->query($sql_set);

    $sql_member = "SELECT * FROM member WHERE mb_id =" . $_SESSION["id"];
    $result_member = $db_con->query($sql_member);
    $row_member = $result_member->fetch_array(MYSQLI_BOTH);
    ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!--Header-->
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand text-white" href="../index.php">
                <img src="/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
                รวมโต๊ะ . com
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="list.php"> <i class="fa fa-map-marker" aria-hidden="true"></i>
                            บริเวณใกล้เคียง</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="list.php"><i class="fa fa-tags"></i> ร้านแนะนำ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="order.php"><i class="fa fa-shopping-cart"></i> รายการจอง
                            <span class="badge badge-light">1</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-phone"></i> ติดต่อเรา</a>
                    </li>
                    <?php
                    if (!isset($_SESSION['id'])) {
                        ?>
                        <li class="nav-item">
                            <a class="btn btn-danger" href="login.html"><i class="fa fa-sign-in"></i> เข้าสู่ระบบ</a>
                        </li>
                    <?php
                    } else {
                        ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                โปรไฟล์
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="profile/profile.php">ชื่อผู้ใช้</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="login/logout.php">ออกจากระบบ</a>
                            </div>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

    <!--body-->
    <div class="container">

        <br>
        <div class="alert alert-light" role="alert">
            หน้าแรก / รายการ้านจัดงาน
        </div>

        <div class="card">
            <div class="row no-gutters">
                <div class="col-md-8">
                    <img src="<?php echo $row_store['st_image'] ?>" class="card-img" alt="img" style="height: 100%">
                </div>
                <div class="col-md-4">
                    <div class="card-body">

                        <h3 class="card-title"><?php echo $row_store['st_name'] ?></h3>
                        <span class="text-left">ประเภท : <?php echo $row_store['st_type'] ?></span>
                        <p class="card-text text-right">
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <i class="text-warning fa fa-star"></i>
                            <small class="text-muted"><?php echo $row_store['st_score'] ?></small>
                        </p>

                        <hr>

                        <strong class="card-text">ข้อมูลสถานที่</strong>
                        <p><?php echo $row_store['st_location'] ?> <?php echo $row_store['st_province'] ?>
                            <?php echo $row_store['st_zipcode'] ?></p>
                    </div>
                    <img src="../assets/map.png" class="card-img" alt="img">
                </div>
            </div>

            <br>

            <div class="card-body">
                <form>
                    <div class="form-group">
                        <h3>ข้อมูลผู้จอง</h3>
                        <div class="row form-group">
                            <div class="col-12 col-sm-6">
                                <label for="name">ชื่อ-นามสกุล (ผู้โอนเงิน)</label>
                                <input type="text" id="name" class="form-control" placeholder="ชื่อ-นามสกุล" value="<?php echo $row_member['mb_firstname']; ?> &nbsp; <?php echo $row_member['mb_lastname']; ?>">
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="telphone">เบอร์ติดต่อ</label>
                                <input type="number" id="telphone" class="form-control" placeholder="เบอร์ติดต่อ" value="<?php echo $row_member['mb_telphone']; ?>">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12 col-sm-6">
                                <label for="formGroupExampleInput">สถานที่จัดงาน</label>
                                <textarea type="text" class="form-control" placeholder="รายละเอียดตำแหน่งสถานที่จัดงาน" rows="5"><?php echo $row_member['mb_address']; ?></textarea>
                            </div>

                            <div class="col-12 col-sm-6">
                                <label for="province">จังหวัด</label>
                                <input type="text" id="province" class="form-control" placeholder="จังหวัด" value="<?php echo $row_member['mb_province']; ?>">
                                <br>
                                <label for="zipcode">รหัสไปรษณีย์</label>
                                <input type="text" id="zipcode" class="form-control" placeholder="รหัสไปรษณีย์" value="<?php echo $row_member['mb_zipcode']; ?>" disabled>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <br>

                    <div class="form-group">
                        <h3>รายการอาหาร</h3>
                    </div>

                    <div class="container">
                        <div class="row">
                            <?php
                            while ($row_set = mysqli_fetch_array($result_set)) {
                                ?>
                                <div class="col-3">
                                    <div class="card">
                                        <form action="food_menu.php" method="post">
                                            <img src="<?php echo $row_set['s_image'] ?>" class="card-img-top" alt="...">
                                            <div class="card-body text-secondary">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h6 class="card-title"><?php echo $row_set['s_name'] ?></h6>
                                                    </div>
                                                    <div class="col-12 text-right font-weight-light">
                                                        <small class="card-text" style="font-size: 12px">ราคา
                                                            <?php echo $row_set['s_price'] ?> บาท/โต๊ะ
                                                        </small>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn"name="id" value="<?php echo $row_set['s_id']; ?>">เลือก</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>

                    <hr>

                    <br><br>

                    <h3>รายการที่คุณสั่ง</h3>

                    <div class="alert alert-light collapse" id="myAlert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <div class="card mb-3 text-decoration-none" style="color: black;">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <img src="../assets/menu-1100.png" class="card-img" alt="image" style="height: 100%">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title">ชุดที่ 1</h5>
                                        <strong>เมนูอาหาร : </strong>
                                        <pre>
    1.ข้าวเกรียบ
    2.ขนมจีน
    3.กระเพาะปลาน้ำแดงทรงเครื่อง 
    4.ยำทะเล
    5.ปลาทับทิมทอดยำมะม่วง
    6.ข้าวผัดปูใส้กรอก
    7.ต้มยำทะเลน้ำข้นรวมมิตร
    8.วุ้นมะพร้าวอ่อน
                                        </pre>
                                        <div style="text-align: right;">
                                            <h5 class="card-title">จำนวน 10 โต๊ะ</h5>
                                            <h5 class="card-title">ค่าใช้จ่ายทั้งหมด :
                                                <?php echo $row_set['s_price'] * 10 ?> บาท</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <script>
                        $(document).ready(function() {
                            $('#btnSelect').click(function() {
                                $('#myAlert').show()
                            })
                        });
                    </script>

                    <hr>

                    <br><br>

                    <div class="row form-group">
                        <div class="col-12 col-sm-6 mb-2">
                            <h4>ค่าใช้จ่ายทั้งหมด : <?php echo $row_set['s_price'] ?> บาท จำนวน 10 โต๊ะ </h4>
                            <p>เฉลี่ยโต๊ะ 1,100 บาท / 1 โต๊ะสามารถนั้งได้ 8 คน </p>

                        </div>
                        <div class="col-12 col-sm-6" style="text-align: right;">
                            <a name="" id="" class="btn btn-danger btn-block" href="payment.php" role="button">จอง</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <br>
        <br>



        <div class="row">
            <div class="col-12 col-sm-8">
                <div class="card">
                    <!--card body-->
                    <div class="card-body">
                        <h3 class="card-title">รีวิว</h3>
                        <!--title-->
                        <div class="row">
                            <div class="col-4 col-sm-2 text-right">
                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid" style="height: 4rem" />
                            </div>
                            <div class="col-8 col-sm-10">
                                <div class="form-group">
                                    <div class="row align-self-center">
                                        <div class="col-12 col-sm-6">
                                            <p><strong class="text-secondary text-left">Mr. Kittikorn
                                                    Jitprapajin</strong>
                                        </div>
                                        <div class="col-12 col-sm-6 text-right">
                                            star
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-10 offset-sm-2">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3" id="comment" placeholder="แสดงความคิดเห็น"></textarea>
                                    <div class="text-right">
                                        <a name="" id="" class="btn btn-light btn-sm" href="">แสดงความคิดเห็น</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <!--Commant-->
                        <div class="row">
                            <div class="col-4 col-sm-2">
                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid" />

                            </div>
                            <div class="col-8 col-sm-10 align-self-center">
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <strong>Maniruzzaman
                                            Akash</strong>
                                    </div>
                                    <div class="col-12 col-sm-6 text-right">
                                        <small>15 Minutes Ago</small>
                                        <p>
                                            <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                            <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                            <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                            <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-10 offset-sm-2">
                                <p>จัดงานดีมากเลยค่ะ อาหารอร่อยมากๆเลย ชอบมากที่สุดก็ต้มยำน้ำข้น ^^</p>
                                <p>
                                    <a href="#" class="float-right btn btn-outline-warning ml-2 btn-sm"> <i class="fa fa-reply"></i>
                                        Reply</a>
                                    <a href="#" class="float-right btn text-white btn-danger btn-sm"> <i class="fa fa-heart"></i>
                                        Like</a>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--Commant-->
        </div>
        <!--card body-->
    </div>

    <br>

    <!-- Footer -->
    <footer class="page-footer font-small bg-dark" style="color: white">
        <div class="container">
            <div class="container-fluid text-center text-md-left">
                <hr>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6">
                        <!--title-->
                        <h3 class="text-uppercase text-danger">รวมโต๊ะ</h3>
                        <h3 class="text-uppercase text-danger">.COM</h3>
                        <!--title-->
                        <hr>
                        <!--Detail-->
                        <p>ศูนย์รวมการจัดงานเลี้ยง ค้นหาเมนูที่คุณต้องการในราคาที่ถูกใจ</p>
                        <p>ได้ที่ รวมโต๊ะ.com เรายินดีให้คำแนะนำ</p>
                        <!--Detail-->
                    </div>
                    <hr class="clearfix w-100 d-md-none pb-6">
                    <!--Link-->
                    <div class="col-12 col-md-6 col-lg-3">
                        <br>
                        <ul class="list-unstyled">
                            <li>
                                <a href="index.php" class="text-white text-decoration-none"><i class="fa fa-home"></i>
                                    &nbsp;&nbsp;หน้าแรก</a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-shopping-cart"></i> &nbsp;&nbsp;รายการที่จอง <span class="badge badge-light">1</span></a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;บริเวณใกล้เคียง</a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-tags"></i>
                                    &nbsp;ร้านแนะนำ</a>
                            </li>
                            <li>
                                <a href="#!" class="text-white text-decoration-none"><i class="fa fa-phone"></i>
                                    &nbsp;&nbsp;ติดต่อเรา</a>
                            </li>
                        </ul>
                    </div>
                    <!--Link-->

                    <div class="col-12 col-md-6 col-lg-3">
                        <br>
                        <p>
                            <a href="!#" class="text-primary"><i class="fa fa-facebook-square fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-warning"><i class="fa fa-instagram fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-success"><i class="fa fa-phone-square fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-danger"><i class="fa fa-google-plus-square fa-2x"></i></a> &nbsp;
                        </p>

                    </div>
                </div>
            </div>
            <!--Copyright-->
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="https://รวมโต๊ะ.com" class="text-danger text-decoration-none"> รววมโต๊ะ.com</a>
            </div>
        </div>
    </footer>
    <!-- Footer -->

</body>

</html>