<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>List</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/card.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

    <!--navbar-->
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand text-white" href="../index.php">
                <img src="/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30"
                    class="d-inline-block align-top" alt="">
                รวมโต๊ะ . com
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="list.php"> <i class="fa fa-map-marker" aria-hidden="true"></i>
                            บริเวณใกล้เคียง</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="list.php"><i class="fa fa-tags"></i> ร้านแนะนำ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="order.php"><i class="fa fa-shopping-cart"></i> รายการจอง
                            <span class="badge badge-light">1</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-phone"></i> ติดต่อเรา</a>
                    </li>
                    <?php
                        if (!isset($_SESSION['id'])) {
                            ?>
                    <li class="nav-item">
                        <a class="btn btn-danger" href="login.html"><i class="fa fa-sign-in"></i> เข้าสู่ระบบ</a>
                    </li>
                    <?php
                        }else {
                            ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            โปรไฟล์
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="profile/profile.php">ชื่อผู้ใช้</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="login/logout.php">ออกจากระบบ</a>
                        </div>
                    </li>
                    <?php
                        }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

    <!--content-->

    <br>
    </br>

    <div class="container">

        <div class="alert alert-light" role="alert">
            หน้าแรก / ค้นหา รายการ้านจัดงาน
        </div>

        <form class="col-12">
            <div class="card card-signin card-body">
                <div class="row">
                    <div class="form-group col-12">
                        <h4>ค้นหา</h4>
                    </div>
                    <div class="form-group col-6 col-sm-4">
                        <label for="budget">งบประมาณ</label>
                        <input type="number" class="form-control" id="budget" placeholder="กรอกงบประมาณที่ท่านต้องการ">
                    </div>
                    <div class="form-group col-6 col-sm-4">
                        <label for="people">จำนวนผู้ร่วมงาน</label>
                        <input type="number" class="form-control" id="people" placeholder="จำนวนผู้ร่วมงาน">
                    </div>
                    <div class="form-group col-12 col-sm-4">
                        <label for="place">สถานที่</label>
                        <select type="text" class="form-control" id="place">
                            <option>--------เลือกจังหวัด--------</option>
                            <option>กรุงเทพ</option>
                            <option>ขอนแก่น</option>
                            <option>มหาสารคาม</option>
                            <option>เชียงใหม่</option>
                        </select>
                    </div>
                </div>

                <div style="text-align: right">
                    <button type="submit" class="btn btn-danger">ค้นหา</button>
                </div>
            </div>
        </form>

        <br>
        <div class="card text-left">
            <img class="card-img-top" src="holder.js/100px180/" alt="">
            <div class="card-body">
                <h3 class="card-title">ร้านแนะนำ</h3>
                <hr>
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <!-- Card -->
                        <div class="card booking-card">

                            <div class="card-header font-weight-bold bg-danger text-white">
                                ร้านจัดงาน...
                            </div>

                            <!-- Card image -->
                            <div class="view overlay">
                                <img class="card-img-top"
                                    src="https://traveleastthailand.com/wp-content/uploads/2017/07/WV2A3205.jpg"
                                    style="height: 12rem;" alt="Card image cap">
                            </div>

                            <!-- Card content -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <!-- Title -->
                                        <h6 class="card-title">• สถานที่...</h6>
                                    </div>
                                    <!-- Data -->
                                    <div class="col-6" style="text-align: right;">
                                        <!-- <ul class="list-unstyled list-inline rating mb-0">
                                                <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"> </i></li>
                                                <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"></i></li>
                                                <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"></i></li>
                                                <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                                                <li class="list-inline-item">
                                                </li>
                                            </ul> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="text-muted font-weight-lighter" style="font-size: 12px;">4.5 (413)</p>
                                    </div>
                                    <div class="col-6" style="text-align: right;">
                                        <a name="" id="" class="btn btn-outline-danger btn-sm" href="detail.php"
                                            role="button" style="font-size: 12px;">รายละเอียด</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <!-- Card -->
                        <div class="card booking-card">

                            <div class="card-header font-weight-bold bg-danger text-white">
                                ร้านจัดงาน...
                            </div>

                            <!-- Card image -->
                            <div class="view overlay">
                                <img class="card-img-top"
                                    src="https://traveleastthailand.com/wp-content/uploads/2017/07/WV2A3205.jpg"
                                    style="height: 12rem;" alt="Card image cap">
                                <a href="#!">
                                    <div class="mask rgba-white-slight"></div>
                                </a>
                            </div>

                            <!-- Card content -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <!-- Title -->
                                        <h6 class="card-title">• สถานที่...</h6>
                                    </div>
                                    <!-- Data -->
                                    <div class="col-6" style="text-align: right;">
                                        <!-- <ul class="list-unstyled list-inline rating mb-0">
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"> </i></li>
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"></i></li>
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"></i></li>
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                                                    <li class="list-inline-item">
                                                    </li>
                                                </ul> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="text-muted font-weight-lighter" style="font-size: 12px;">4.5 (413)</p>
                                    </div>
                                    <div class="col-6" style="text-align: right;">
                                        <a name="" id="" class="btn btn-outline-danger btn-sm" href="detail.php"
                                            role="button" style="font-size: 12px;">รายละเอียด</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <!-- Card -->
                        <div class="card booking-card">

                            <div class="card-header font-weight-bold bg-danger text-white">
                                ร้านจัดงาน...
                            </div>

                            <!-- Card image -->
                            <div class="view overlay">
                                <img class="card-img-top"
                                    src="https://traveleastthailand.com/wp-content/uploads/2017/07/WV2A3205.jpg"
                                    style="height: 12rem;" alt="Card image cap">
                                <a href="#!">
                                    <div class="mask rgba-white-slight"></div>
                                </a>
                            </div>

                            <!-- Card content -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <!-- Title -->
                                        <h6 class="card-title">• สถานที่...</h6>
                                    </div>
                                    <!-- Data -->
                                    <div class="col-6" style="text-align: right;">
                                        <!-- <ul class="list-unstyled list-inline rating mb-0">
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"> </i></li>
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"></i></li>
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"></i></li>
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                                                    <li class="list-inline-item">
                                                    </li>
                                                </ul> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="text-muted font-weight-lighter" style="font-size: 12px;">4.5 (413)</p>
                                    </div>
                                    <div class="col-6" style="text-align: right;">
                                        <a name="" id="" class="btn btn-outline-danger btn-sm" href="detail.php"
                                            role="button" style="font-size: 12px;">รายละเอียด</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-3">
                        <!-- Card -->
                        <div class="card booking-card">

                            <div class="card-header font-weight-bold bg-danger text-white">
                                ร้านจัดงาน...
                            </div>

                            <!-- Card image -->
                            <div class="view overlay">
                                <img class="card-img-top"
                                    src="https://traveleastthailand.com/wp-content/uploads/2017/07/WV2A3205.jpg"
                                    style="height: 12rem;" alt="Card image cap">
                            </div>

                            <!-- Card content -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <!-- Title -->
                                        <h6 class="card-title">• สถานที่...</h6>
                                    </div>
                                    <!-- Data -->
                                    <div class="col-6" style="text-align: right;">
                                        <!-- <ul class="list-unstyled list-inline rating mb-0">
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"> </i></li>
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"></i></li>
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star amber-text"></i></li>
                                                    <li class="list-inline-item mr-0"><i class="fas fa-star"></i></li>
                                                    <li class="list-inline-item">
                                                    </li>
                                                </ul> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <p class="text-muted font-weight-lighter" style="font-size: 12px;">4.5 (413)</p>
                                    </div>
                                    <div class="col-6" style="text-align: right;">
                                        <a name="" id="" class="btn btn-outline-danger btn-sm" href="detail.php"
                                            role="button" style="font-size: 12px;">รายละเอียด</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>

    </div>

    <!-- Footer -->
    <footer class="page-footer font-small bg-dark" style="color: white">
        <div class="container">
            <div class="container-fluid text-center text-md-left">
                <hr>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6">
                        <!--title-->
                        <h3 class="text-uppercase text-danger">รวมโต๊ะ</h3>
                        <h3 class="text-uppercase text-danger">.COM</h3>
                        <!--title-->
                        <hr>
                        <!--Detail-->
                        <p>ศูนย์รวมการจัดงานเลี้ยง ค้นหาเมนูที่คุณต้องการในราคาที่ถูกใจ</p>
                        <p>ได้ที่ รวมโต๊ะ.com เรายินดีให้คำแนะนำ</p>
                        <!--Detail-->
                    </div>
                    <hr class="clearfix w-100 d-md-none pb-6">
                    <!--Link-->
                    <div class="col-12 col-md-6 col-lg-3">
                        <br>
                        <ul class="list-unstyled">
                            <li>
                                <a href="index.php" class="text-white text-decoration-none"><i class="fa fa-home"></i>
                                    &nbsp;&nbsp;หน้าแรก</a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i
                                        class="fa fa-shopping-cart"></i> &nbsp;&nbsp;รายการที่จอง <span
                                        class="badge badge-light">1</span></a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-map-marker"
                                        aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;บริเวณใกล้เคียง</a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-tags"></i>
                                    &nbsp;ร้านแนะนำ</a>
                            </li>
                            <li>
                                <a href="#!" class="text-white text-decoration-none"><i class="fa fa-phone"></i>
                                    &nbsp;&nbsp;ติดต่อเรา</a>
                            </li>
                        </ul>
                    </div>
                    <!--Link-->

                    <div class="col-12 col-md-6 col-lg-3">
                        <br>
                        <p>
                            <a href="!#" class="text-primary"><i class="fa fa-facebook-square fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-warning"><i class="fa fa-instagram fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-success"><i class="fa fa-phone-square fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-danger"><i class="fa fa-google-plus-square fa-2x"></i></a> &nbsp;
                        </p>

                    </div>
                </div>
            </div>
            <!--Copyright-->
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="https://รวมโต๊ะ.com" class="text-danger text-decoration-none"> รววมโต๊ะ.com</a>
            </div>
        </div>
    </footer>
    <!-- Footer -->

</body>

</html>