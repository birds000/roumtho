<?php
session_start();
?>
<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/card.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="sweetalert2.all.min.js"></script>
    <!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

</head>

<body>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <?php
        include("connectDB.php");

        $sql_member = "SELECT * FROM member WHERE mb_id = 17";
        $result_member = $db_con->query($sql_member);
        $row_member = $result_member->fetch_array(MYSQLI_BOTH);

        $sql_store = "SELECT * FROM store WHERE st_id = 1";
        $result_store = $db_con->query($sql_store);
        $row_store = $result_store->fetch_array(MYSQLI_BOTH);

        $sql_order = "SELECT * FROM `order`";
        $result_order = $db_con->query($sql_order);
        $row_order = $result_order->fetch_array(MYSQLI_BOTH);

        $sql_order_menu = "SELECT * FROM order_menu WHERE odm_id = 1";
        $result_order_menu = $db_con->query($sql_order_menu);
        $row_order_menu = $result_order_menu->fetch_array(MYSQLI_BOTH);

        $sql_set = "SELECT * FROM `set`";
        $result_set = $db_con->query($sql_set);
        $row_set = $result_set->fetch_array(MYSQLI_BOTH);

        $sql_dish = "SELECT * FROM `dish` WHERE s_id = 1001";
        $result_dish = $db_con->query($sql_dish);
        $row_dish = $result_dish->fetch_array(MYSQLI_BOTH);

        $sql_food = "SELECT * FROM `food` WHERE d_id = 1011";
        $result_food = $db_con->query($sql_food);
        $row_food = $result_food->fetch_array(MYSQLI_BOTH);
    ?>

    <!--Header-->
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand text-white" href="../index.php">
                <img src="/docs/4.3/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
                รวมโต๊ะ . com
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="list.php"> <i class="fa fa-map-marker" aria-hidden="true"></i> บริเวณใกล้เคียง</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="list.php"><i class="fa fa-tags"></i> ร้านแนะนำ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="order.php"><i class="fa fa-shopping-cart"></i> รายการจอง
                            <span class="badge badge-light">1</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-phone"></i> ติดต่อเรา</a>
                    </li>
                    <?php
                    if (!isset($_SESSION['id'])) {
                        ?>
                        <li class="nav-item">
                            <a class="btn btn-danger" href="login.html"><i class="fa fa-sign-in"></i> เข้าสู่ระบบ</a>
                        </li>
                    <?php
                    } else {
                        ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                โปรไฟล์
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="profile/profile.php">ชื่อผู้ใช้</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="login/logout.php">ออกจากระบบ</a>
                            </div>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

    <!--body-->
    <div class="container">
        <br>
        <div class="alert alert-light" role="alert">
            หน้าแรก / รายการจอง / ช่องทางการชำระเงิน
        </div>

        <div class="card mb-3 text-decoration-none" style="color: black;">
            <div class="row no-gutters">
                <div class="col-md-4">
                    <img src="<?php echo $row_store['st_image']; ?>" class="card-img" alt="image" style="height: 100%">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-7">
                                <h5 class="card-title">ชื่อร้าน : <?php echo $row_store['st_name']; ?></h5>
                                <strong>ประเภทร้าน : <?php echo $row_store['st_type']; ?></strong>
                            </div>
                            <div class="col-5">
                                <p class="card-text" style="text-align: right;"><small class="text-muted"><strong>วันที่เวลาจอง</strong> <?php echo $row_order['od_datetime']; ?></small></p>
                            </div>
                        </div>
                        <p class="card-text"><strong>เมนูอาหาร : </strong> 1.ข้าวเกรียบ 2.ขนมจีน
                            3.กระเพาะปลาน้ำแดงทรงเครื่อง 4.ยำทะเล
                            5.ปลาทับทิมทอดยำมะม่วง 6.ข้าวผัดปูใส้กรอก 7.ต้มยำทะเลน้ำข้นรวมมิตร 8.วุ้นมะพร้าวอ่อน</p>

                        <div style="text-align: right;">
                            <h5 class="card-title">ค่าใช้จ่ายทั้งหมด : <?php echo $row_order['od_amount_paid']; ?> บาท</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card text-left">
            <div class="card-body">
                <h2 class="card-title">ช่องทาง</h2>
                <h4 class="card-title">การชำระเงิน</h4>
                <p class="card-text">โปรดโอนเงินค่ามัดจำตามบัญชีด้านล่าง</p>
                <hr>
                <h4 class="card-title text-muted">ชื่อบัญชี : บริษัท รวมโต๊ะ จำกัด</h4>

                <form>
                    <div class="custom-control custom-radio form-group">
                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio1">
                            <span class="text-center" style="padding: 0px 30px 20px 30px;"><img src="../assets/bank/logo1.png" style="height: 2rem; width: 2rem;" /></span>
                            <span class="text-center" style="padding: 0px 30px 0px 30px; width: 50px;">ออมสิน</span>
                            <span class="text-left" style="padding: 0px 30px 0px 53px;">ประเภทบัญชี : ออมทรัพย์</span>
                            <span class="text-left" style="padding: 0px 30px 0px 30px;">เลขที่บัญชี :
                                020-244-978-530</span>
                        </label>
                    </div>

                    <div class="custom-control custom-radio form-group">
                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio2">
                            <span class="text-center" style="padding: 0px 30px 0px 30px;"><img src="../assets/bank/logo2.jpg" style="height: 2rem; width: 2rem;" /></span>
                            <span class="text-center" style="padding: 0px 30px 0px 30px;">ไทยพาณิช</span>
                            <span class="text-left" style="padding: 0px 30px 0px 30px;">ประเภทบัญชี : ออมทรัพย์</span>
                            <span class="text-left" style="padding: 0px 30px 0px 30px;">เลขที่บัญชี :
                                404-845-618-8</span>
                        </label>
                    </div>

                    <div class="custom-control custom-radio form-group">
                        <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio3">
                            <span class="text-center" style="padding: 0px 30px 0px 30px;"><img src="../assets/bank/logo3.jpg" style="height: 2rem; width: 2rem;" /></span>
                            <span class="text-center" style="padding: 0px 30px 0px 30px;">กรุงเทพฯ</span>
                            <span class="text-left" style="padding: 0px 30px 0px 40px;">ประเภทบัญชี : ออมทรัพย์</span>
                            <span class="text-left" style="padding: 0px 30px 0px 30px;">เลขที่บัญชี :
                                630-013-745-9</span>
                        </label>
                    </div>

                    <div class="custom-control custom-radio form-group">
                        <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input" data-toggle="modal" data-target="#exampleModalCenter">
                        <label class="custom-control-label" for="customRadio4">
                            <span class="text-center" style="padding: 0px 30px 0px 30px;"><img src="../assets/bank/promptpay.jpg" style="height: 2rem; width: 2rem;" /></span>
                            <span class="text-center" style="padding: 0px 30px 0px 30px;">พร้อมเพย์</span>
                            <span class="text-left" style="padding: 0px 30px 0px 38px;">ประเภทบัญชี : ออมทรัพย์</span>
                            <span class="text-left" style="padding: 0px 30px 0px 30px;">เลขที่บัญชี : 091-4453-116</span>
                        </label>
                    </div>

                    <hr>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered bd-example-modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">ชำระผ่าน QR-Code Payment</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-12 col-md-5 text-center">
                                            <img style="width: 100%" src="https://promptpay.io/piyanat/25000.png">
                                        </div>
                                        <div class="col-12 col-md-7 ">
                                            <p><span class="font-weight-bold">ชื่อบัญชี : </span>บริษัท รวมโต๊ะ จำกัด</p>
                                            <p><span class="font-weight-bold">ธนาคาร : </span>ออมสิน</p>
                                            <p><span class="font-weight-bold">ประเภทบัญชี : </span>ออมทรัพย์</p>
                                            <p><span class="font-weight-bold">พร้อมเพย์ : </span>091-4453-116</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="row form-group">
                        <div class="col-12 col-sm-6">
                            <label for="formGroupExampleInput">ชื่อ-นามสกุล (ผู้โอนเงิน)</label>
                            <input type="text" class="form-control" placeholder="ชื่อ-นามสกุล" value="<?php echo $row_member['mb_firstname']; ?> &nbsp; <?php echo $row_member['mb_lastname'] ?>">
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="formGroupExampleInput">เบอร์ติดต่อ</label>
                            <input type="number" class="form-control" placeholder="เบอร์ติดต่อ" value="<?php echo $row_member['mb_telphone']; ?>">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-12 col-sm-6">
                            <label for="formGroupExampleInput">จำนวนเงินที่ต้องชำระ</label>
                            <input type="number" class="form-control" placeholder="จำนวนเงินที่ชำระ" value="<?php echo $row_order['od_amount_paid']; ?>" disabled>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="formGroupExampleInput">วันที่ชำระเงิน</label>
                            <input type="date" class="form-control" data-date-end-date="0d">
                        </div>
                    </div>

                    <div class="custom-file form-group">
                        <input type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">หลักฐานการชำระเงิน</label>
                    </div>

                    <hr>

                    <small class="text-danger">**หมายเหตุ : หากท่านทำรายการแล้วแต่ยังไม่ได้ชำระเงินภายใน 7
                        วัน ทางระบบจะทำการยกเลิกการจองการจัดงานอัตโนมัติ </small>
                    <br>

                    <div class="form-group">
                        <a id="btnSubmit" class="btn btn-warning btn-lg btn-block">ชำระเงิน</a>
                    </div>

                    <div id="AlertSuccess" class="alert alert-success collapse form-group">
                        <a href="#" class="close" data-dismiss="alert">&times</a>
                        <strong>ทำรายการเสร็จสิ้น</strong> กรุณารอการแจ้งผลทาง SMS ภายใน 15-30 นาที.
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('#btnSubmit').click(function() {
                                Swal.fire({
                                    type: 'success',
                                    title: 'ทำรายการเสร็จสิ้น',
                                    text: 'กรุณารอการแจ้งผลทาง SMS ภายใน 15-30 นาที.',
                                    showConfirmButton: false
                                })
                            });
                        });
                    </script>

                </form>
            </div>
        </div>
    </div>
    <br>

    <!-- Footer -->
    <footer class="page-footer font-small bg-dark" style="color: white">
        <div class="container">
            <div class="container-fluid text-center text-md-left">
                <hr>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6">
                        <!--title-->
                        <h3 class="text-uppercase text-danger">รวมโต๊ะ</h3>
                        <h3 class="text-uppercase text-danger">.COM</h3>
                        <!--title-->
                        <hr>
                        <!--Detail-->
                        <p>ศูนย์รวมการจัดงานเลี้ยง ค้นหาเมนูที่คุณต้องการในราคาที่ถูกใจ</p>
                        <p>ได้ที่ รวมโต๊ะ.com เรายินดีให้คำแนะนำ</p>
                        <!--Detail-->
                    </div>
                    <hr class="clearfix w-100 d-md-none pb-6">
                    <!--Link-->
                    <div class="col-12 col-md-6 col-lg-3">
                        <br></br>
                        <ul class="list-unstyled">
                            <li>
                                <a href="index.php" class="text-white text-decoration-none"><i class="fa fa-home"></i> &nbsp;&nbsp;หน้าแรก</a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-shopping-cart"></i> &nbsp;&nbsp;รายการที่จอง <span class="badge badge-light">1</span></a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;บริเวณใกล้เคียง</a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-tags"></i> &nbsp;ร้านแนะนำ</a>
                            </li>
                            <li>
                                <a href="#!" class="text-white text-decoration-none"><i class="fa fa-phone"></i> &nbsp;&nbsp;ติดต่อเรา</a>
                            </li>
                        </ul>
                    </div>
                    <!--Link-->

                    <div class="col-12 col-md-6 col-lg-3">
                        <br></br>
                        <p>
                            <a href="!#" class="text-primary"><i class="fa fa-facebook-square fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-warning"><i class="fa fa-instagram fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-success"><i class="fa fa-phone-square fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-danger"><i class="fa fa-google-plus-square fa-2x"></i></a> &nbsp;
                        </p>

                    </div>
                </div>
            </div>
            <!--Copyright-->
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="https://รวมโต๊ะ.com" class="text-danger text-decoration-none"> รววมโต๊ะ.com</a>
            </div>
        </div>
    </footer>
    <!-- Footer -->

</body>

</html>