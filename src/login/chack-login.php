<?php
session_start();
include '../../config/Database.php';

$strSQL = "SELECT * FROM member WHERE mb_username = '" . mysqli_real_escape_string($conn, $_POST['Username']) . "' 
    AND mb_password = '" . mysqli_real_escape_string($conn, $_POST['Password']) . "'";
$objQuery = mysqli_query($conn, $strSQL);
$objResult = mysqli_fetch_array($objQuery, MYSQLI_ASSOC);
if (!$objResult) {
    ?>
    <script>
        alert(" User หรือ  Password ไม่ถูกต้อง ");
        location.replace("../login.html");
    </script>
<?php
} else {
    $_SESSION["id"] = $objResult["mb_id"];
    $_SESSION["firstname"] = $objResult["mb_firstname"];
    $_SESSION["lastname"] = $objResult["mb_lastname"];
    
    session_write_close();

    header("location:../../index.php");
}
mysqli_close($conn);
?>