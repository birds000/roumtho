<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

    <?php
        include('connectDB.php');
    
        $sql_store = "SELECT * FROM store";
        $result_store = $db_con->query($sql_store);
        $row_store = $result_store->fetch_array();

        $id = $_POST['id'];

        $sql_set = "SELECT * FROM `set` WHERE s_id =".$id;
        $result_set = $db_con->query($sql_set);
    
        $sql_dish = "SELECT * FROM dish WHERE s_id = ".$id;
        $result_dish = $db_con->query($sql_dish);
        echo $sql_dish;
    
        $sql_member = "SELECT * FROM member";
        $result_member = $db_con->query($sql_member);
        $row_member = $result_member->fetch_array(MYSQLI_BOTH);
    ?>

    <div class="container">
        <form>
            <div class="form-row">
                <?php while ($row_dish = mysqli_fetch_assoc($result_dish)){ ?>
                <div class="form-group col-md-6">
                    <label><?php echo $row_dish['d_name'] ?> :</label>
                    <div class="form-group">
                        <select class="form-control">
                            <?php 
                                $d_rs = $row_dish['d_id'];
                            
                                $sql_food = "SELECT * FROM `food` WHERE d_id = '$d_rs' ORDER BY d_id";
                                $result_food = $db_con->query($sql_food);
    
                                while ($row_food = mysqli_fetch_assoc($result_food)) { 
                            ?>
                            <option><?php echo $row_food['f_name']; ?></option>
                            <?php 
                                } 
                            ?>
                        </select>
                    </div>
                </div>
                <?php
                    }
                ?>
            </div>
            <div class="form-row">
                <?php $row_set = mysqli_fetch_array($result_set) ?>
                <div class="form-group col-md-6">
                    <label>จำนวนโต๊ะ :</label>
                    <input type="number" class="form-control" id="inputTable" value="10">
                    <button type="submit" on>คำนวน</button>
                </div>
                <div class="form-group col-md-6">
                    <div class="text-right">
                        <br>
                        <p>ราคา <?php echo $row_set['s_price'] ?> บาท/โต๊ะ</p>
                        <p>รวมทั้งหมด <?php echo $row_set['s_price']*10 ?> บาท/โต๊ะ</p>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-block">ยืนยน</button>
            <button type="submit" class="btn btn-secondary btn-block">ยกเลิก</button>
        </form>
    </div>

</body>

</html>