<?php
session_start();
?>
<!doctype html>
<html lang="en">

<head>
    <title>Home</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

    <?php
    include('src/connectDB.php');

    $sql_store = "SELECT * FROM store ORDER BY st_id;";

    $sql_store_pop = "SELECT * FROM store WHERE st_score = '5' ORDER BY st_id LIMIT 2;";
    ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <!--Navbar-->
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-danger">
        <div class="container">
            <a class="navbar-brand text-white" href="index.php">
                รวมโต๊ะ . com
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav ml-auto">
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="src/list.php"> <i class="fa fa-map-marker" aria-hidden="true"></i> บริเวณใกล้เคียง</a>
                    </li> -->
                    <li class="nav-item">
                        <a href="#restaurant_popular" class="nav-link" href="src/list.php"><i class="fa fa-tags"></i> ร้านแนะนำ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="src/order.php"><i class="fa fa-shopping-cart"></i> รายการจอง
                            <span class="badge badge-light">1</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer"><i class="fa fa-phone"></i> ติดต่อเรา</a>
                    </li>
                    <?php
                    if (!isset($_SESSION['id'])) {
                        ?>
                        <li class="nav-item">
                            <a class="btn btn-light" href="src/login.html"><i class="fa fa-sign-in"></i> เข้าสู่ระบบ</a>
                        </li>
                    <?php
                    } else {
                        ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                โปรไฟล์
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="src/profile/profile.php">ชื่อผู้ใช้</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="src/login/logout.php">ออกจากระบบ</a>
                            </div>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>
    <!--Navbar-->

    <!--ค้นหา Header-->
    <div class="iamge-head">
        <br>
        <div class="justify-content-around">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-xl-6"></div>
                    <form class="col-12 col-xl-6" action="index.php#restaurant" name="myForm" onsubmit="return validateForm()" style="text-align: center" method="post">
                        <label class="text-white">
                            <h2>รวมโต๊ะ</h2>
                            <p>ศูนย์รวมการจัดงานเลี้ยง ค้นหาเมนูที่คุณต้องการในราคาที่ถูกใจ</p>
                            <p>ได้ที่ รวมโต๊ะ.com เรายินดีให้คำแนะนำ</p>
                        </label>
                        <br>
                        <div class="card card-body" style="text-align: left">
                            <div class="row">
                                <div class="form-group col-12">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                    <label>ค้นหา</label>
                                </div>
                                <div class="form-group col-6">
                                    <label for="budget"><i class="fa fa-money" aria-hidden="true"></i> งบประมาณ</label>
                                    <input type="number" class="form-control" name="budget" id="budget" placeholder="กรอกงบประมาณที่ท่านต้องการ">
                                </div>
                                <div class="form-group col-6">
                                    <label for="table"><i class="fa fa-user" aria-hidden="true"></i> จำนวนโต๊ะ</label>
                                    <input type="number" class="form-control" name="table" id="table" placeholder="จำนวนโต๊ะ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="place"><i class="fa fa-map-marker" aria-hidden="true"></i> สถานที่</label>
                                <select type="text" class="form-control" name="place" id="place">
                                    <option>ทั้งหมด</option>
                                    <option>กรุงเทพ</option>
                                    <option>ขอนแก่น</option>
                                    <option>มหาสารคาม</option>
                                    <option>เชียงใหม่</option>
                                </select>
                            </div>
                            <div style="text-align: right">
                                <button type="submit" class="btn btn-danger"><i class="fa fa-search" aria-hidden="true"></i> ค้นหา</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--ค้นหา Header-->

    <a name="restaurant_popular"></a>
    <br>
    <br>
    <br>

    <!--เนื้อหา Content-->
    <div class="container">

        <h3>ร้านแนะนำ</h3>
        <!--หัวข้อ-->

        <hr>

        <div class="row">
            <?php
            $result_store_pop = $db_con->query($sql_store_pop);
            while ($row_store_pop = mysqli_fetch_array($result_store_pop)) {

                ?>
                <!--Card 1 ร้านแนะนำ-->
                <div class="col-12 col-xl-6">
                    <a href="src/detail.php" class="card mb-3 text-decoration-none text-dark">
                        <div class="row no-gutters">
                            <!-- Card image -->
                            <div class="col-md-5">
                                <img src="<?php echo $row_store_pop['st_image'] ?>" class="card-img" alt="image" style="height: 100%">
                            </div>
                            <!-- Card content -->
                            <div class="col-md-7">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-7">
                                            <h5 class="card-title"><?php echo $row_store_pop['st_name'] ?></h5>

                                        </div>
                                        <div class="col-5">
                                            <p class="card-text" style="text-align: right;">
                                                <small class="text-muted"><strong>
                                                        <i class="text-warning  fa fa-star"></i>
                                                        <i class="text-warning  fa fa-star"></i>
                                                        <i class="text-warning  fa fa-star"></i>
                                                        <i class="text-warning  fa fa-star"></i>
                                                        <i class="text-warning  fa fa-star-half-o"></i>
                                                        <?php echo $row_store_pop['st_score'] ?>
                                                    </strong></small>
                                            </p>
                                        </div>
                                    </div>
                                    <p><strong>ประเภทร้าน : </strong><?php echo $row_store_pop['st_type'] ?></p>

                                    <p class="card-text"><i class="fa fa-map-marker"></i><strong> สถานที่ : </strong> <?php echo $row_store_pop['st_location'] ?> <?php echo $row_store_pop['st_province'] ?> <?php echo $row_store_pop['st_zipcode'] ?></p>

                                    <div class="row">
                                        <div class="col" style="text-align: right;">
                                            <button class="btn btn-sm btn-danger">รายละอียด</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- Card content -->
                        </div>
                    </a>
                </div>
                <!--Card 1 ร้านแนะนำ-->
            <?php
            }
            ?>
        </div>

        <a name="restaurant"></a>
        <br>
        <br>
        <br>

        <h3>ร้านจัดงาน</h3>
        <!--หัวข้อ---->
        <hr>

        <div class="row">

            <?php

            if (isset($_POST['place']) || isset($_POST['budget']) || isset($_POST['table'])) {
                $sql_store_filter = "SELECT * FROM store WHERE st_province ='" . $_POST['place'] . "'";
                if ($_POST['place'] === "ทั้งหมด") {
                    $result_store = $db_con->query($sql_store);
                } else {
                    $result_store = $db_con->query($sql_store_filter);
                }
            } else {
                $result_store = $db_con->query($sql_store);
            }
            while ($row_store = mysqli_fetch_array($result_store)) {
                ?>
                <!-- Card 1 ร้านใกล้เคียง -->
                <div class="col-md-6 col-lg-3">
                    <form action="src/detail.php" method="POST">
                        <div class="card booking-card">
                            <div class="card-header font-weight-bold bg-danger text-white">
                                <?php echo $row_store['st_name'] ?>
                            </div>
                            <!-- Card image -->
                            <div class="view overlay">
                                <img class="card-img-top" src="<?php echo $row_store['st_image'] ?>" style="height: 12rem;" alt="Card image cap">
                            </div>
                            <!-- Card content -->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="card-title"><i class="fa fa-map-marker"></i><strong>สถานที่</strong> <?php echo $row_store['st_location'] ?> <?php echo $row_store['st_province'] ?> <?php echo $row_store['st_zipcode'] ?></p> <!-- Title -->
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- Star ดาว คะแนนรีวิว -->
                                    <div class="col-6">
                                        <P class="text-muted font-weight-lighter" style="font-size: 12px;">
                                            <i class="text-warning fa fa-star"></i>
                                            <i class="text-warning fa fa-star"></i>
                                            <i class="text-warning fa fa-star"></i>
                                            <i class="text-warning fa fa-star"></i>
                                            <i class="text-warning fa fa-star-half-o"></i>
                                            <?php echo $row_store['st_score'] ?>
                                        </p>
                                    </div>
                                    <!-- Star ดาว คะแนนรีวิว -->
                                    <div class="col-6" style="text-align: right;">
                                        <button type="submit" name="id" value="<?php echo $row_store['st_id']; ?>" class="btn btn-outline-danger btn-sm" role="button" style="font-size: 12px;">รายละเอียด</button>
                                    </div>
                                </div>
                            </div>
                            <!-- Card content -->
                        </div>
                    </form>
                </div>
                <!-- Card 1 ร้านใกล้เคียง -->
            <?php
            }
            ?>
        </div>

        <br>
        <br>
        <!--pagination-->
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center pg-red">
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!--pagination-->
    </div>

    <a name="footer"></a>
    <br>

    <!-- Footer -->
    <footer class="page-footer font-small bg-dark" style="color: white">
        <div class="container">
            <div class="container-fluid text-center text-md-left">
                <hr>
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-6">
                        <!--title-->
                        <h3 class="text-uppercase text-danger">รวมโต๊ะ</h3>
                        <h3 class="text-uppercase text-danger">.COM</h3>
                        <!--title-->
                        <hr>
                        <!--Detail-->
                        <p>ศูนย์รวมการจัดงานเลี้ยง ค้นหาเมนูที่คุณต้องการในราคาที่ถูกใจ</p>
                        <p>ได้ที่ รวมโต๊ะ.com เรายินดีให้คำแนะนำ</p>
                        <!--Detail-->
                    </div>
                    <hr class="clearfix w-100 d-md-none pb-6">
                    <!--Link-->
                    <div class="col-12 col-md-6 col-lg-3">
                        <br></br>
                        <ul class="list-unstyled">
                            <li>
                                <a href="index.php" class="text-white text-decoration-none"><i class="fa fa-home"></i> &nbsp;&nbsp;หน้าแรก</a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-shopping-cart"></i> &nbsp;&nbsp;รายการที่จอง <span class="badge badge-light">1</span></a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp;&nbsp;&nbsp;บริเวณใกล้เคียง</a>
                            </li>
                            <li>
                                <a href="list.php" class="text-white text-decoration-none"><i class="fa fa-tags"></i> &nbsp;ร้านแนะนำ</a>
                            </li>
                            <li>
                                <a href="#!" class="text-white text-decoration-none"><i class="fa fa-phone"></i> &nbsp;&nbsp;ติดต่อเรา</a>
                            </li>
                        </ul>
                    </div>
                    <!--Link-->

                    <div class="col-12 col-md-6 col-lg-3">
                        <br></br>
                        <p>
                            <a href="!#" class="text-primary"><i class="fa fa-facebook-square fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-warning"><i class="fa fa-instagram fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-success"><i class="fa fa-phone-square fa-2x"></i></a> &nbsp;
                            <a href="!#" class="text-danger"><i class="fa fa-google-plus-square fa-2x"></i></a> &nbsp;
                        </p>

                    </div>
                </div>
            </div>
            <!--Copyright-->
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="https://รวมโต๊ะ.com" class="text-danger text-decoration-none"> รววมโต๊ะ.com</a>
            </div>
        </div>
    </footer>
    <!-- Footer -->

    <script>
        function validateForm() {
            var table = document.forms["myForm"]["table"].value;
            var budget = document.forms["myForm"]["budget"].value;
            if (table != "" && budget == "") {
                alert("กรุณากรอกจำนวนงบประมาณก่อน!");
                return false;
            } else if (table == "" && budget != "") {
                alert("กรุณากรอกจำนวนโต๊ะก่อน!");
                return false;
            } else {
                return true;
            }
        }
    </script>

</body>

</html>